import logo from './petani.png'
import book from './book.png'
import search from './search.png'
import facebook from './facebook.png'
import ig from './ig.png'
import tweeter from './tweeter.png'
import './App.css';
import React from 'react'
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  FormControl,
  Button,
  Container,
  img,
  Row,
  Col,
  Card,
  Pagination,
  Media

} from 'react-bootstrap';

function Header(){
  return(
    <div className="napbar">
    <Container>
    <Navbar className="napbar" expand="lg">
  <Navbar.Brand href="#home">
        <Container>
          <img
            alt="logo"
            src={logo}
            width="50"
            height="50"
            className="d-inline-block align-top"
          /> <h3 className="tulisan">Petani Kode</h3>
           </Container>
  </Navbar.Brand>

  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
    <img 
    src={book}
    width="40"
    height="40"
    alt=""
    />
      <NavDropdown title="Tutorial" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">HTML</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Javascript</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Java</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">PHP</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Python</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Git</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">More</NavDropdown.Item>
      </NavDropdown> 
      <Nav.Link href="#link">Buku</Nav.Link>
      <Nav.Link href="#link">Video</Nav.Link>
      <NavDropdown title="Jobs" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Loker Programmer</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Remote Jobs</NavDropdown.Item>
      </NavDropdown>
    </Nav>
 
    <Form inline>
    
      <FormControl type="text" placeholder="Kata Kunci.."  />
      <Button variant="info" ><img src={search} height="25" width="25" alt=""/> </Button>
   
    </Form>

 
  </Navbar.Collapse>
</Navbar>
</Container>
            <div className="jumbo">
         
       <h1 className="head">Belajar Budidaya Kode</h1>
       <h5 className="head">
         Jangan tunggu nanti, tanamlah sekarang biar cepat panen!
       </h5>
       <Button className="lyr"> Mulai belajar! </Button>
      </div>
</div>


    );
}
function Welcome(){
  return(
    <Container  className="head1">

     <h3>Artikel Terbaru</h3>
       <h5>
         Apa aja sih yang baru di Petani Kode?
       </h5>

 <Row>
  {/*card kesatu*/}
      <Col> 
            <Card className="kartu"><a href="#home"> 
             <Card.Img variant="top" src="https://www.petanikode.com/img/cover/ci4.png" />
             </a>
             <Card.Body>
               <Card.Title><a href="#home" style={{ color: 'black' }}>Tutorial Codeigniter 4: Migrasi Database</a></Card.Title>
               <Card.Text>
                  Pelajari cara migrasi database di Codeigniter 4 agar project-mu bisa cepat selesai tanpa hambatan …
               </Card.Text>
             </Card.Body>
            </Card>

       </Col>
 {/*card ke2*/}
      <Col> 
            <Card className="kartu"><a href="#home"> 
             <Card.Img variant="top" src="https://www.petanikode.com/img/cover/pygobject.png" />
             </a>
             <Card.Body>
               <Card.Title><a href="#home" style={{ color: 'black' }}>Belajar Membuat Aplikasi GUI Berbasis Python dan GTK dengan PyGobject</a></Card.Title>
               <Card.Text>
                  Pelajari cara membuat aplikasi GUI berbasis Python dan GTK untuk Linux, Windows, dan MacOS.
               </Card.Text>
             </Card.Body>
            </Card>
      </Col>

 {/*card ke3*/}
      <Col>
            <Card className="kartu"><a href="#home"> 
             <Card.Img variant="top" src="https://www.petanikode.com/img/cover/pygtk-glade.png" />
             </a>
             <Card.Body>
               <Card.Title><a href="#home" style={{ color: 'black' }}>Membuat Aplikasi GUI berbasis Python dengan PyGTK dan Glade</a></Card.Title>
               <Card.Text>
                  Belajar membuat aplikasi GUI berbasis Python dengan Modul PyGTK dan Glade sebagai UI Desainer
               </Card.Text>
             </Card.Body>
            </Card>

      </Col>

</Row>


<Row className="kol">
 {/*card ke4*/}
      <Col> 
            <Card className="kartu"><a href="#home"> 
             <Card.Img variant="top" src="https://www.petanikode.com/img/cover/bottom-nav.png" />
             </a>
             <Card.Body>
               <Card.Title><a href="#home" style={{ color: 'black' }}>Cara Membuat Bottom Navbar Seperti Aplikasi Mobile dengan Bootstrap</a></Card.Title>
               <Card.Text>
                  
                  Gunakan Bottom Navbar agar navigasi websitemu semakin bagus di perangkat mobile.
               </Card.Text>
             </Card.Body>
            </Card>

       </Col>
 {/*card ke5*/}
      <Col> 
            <Card className="kartu"><a href="#home"> 
             <Card.Img variant="top" src="https://www.petanikode.com/img/cover/ci4.png" />
             </a>
             <Card.Body>
               <Card.Title><a href="#home" style={{ color: 'black' }}>Tutorial Codeigniter 4: Membuat Widget dengan View Cells</a></Card.Title>
               <Card.Text>
                  Pelajari cara membuat Template View yang modular dan efektif dengan View Cell di Codeigniter 4.
               </Card.Text>
             </Card.Body>
            </Card>
      </Col>

 {/*card ke6*/}
      <Col>
            <Card className="kartu">
            <a href="#home"> 
             <Card.Img variant="top" src="https://www.petanikode.com/img/cover/ci4.png" />
             </a>
             <Card.Body>
               <Card.Title><a href="#home" style={{ color: 'black' }}>Tutorial Codeigniter 4: Membuat Template yang Efektif dengan View Layout</a></Card.Title>
               <Card.Text>
                 Pelajari cara membuat template yang efektif dengan View Layout.
               </Card.Text>
             </Card.Body>
            </Card>

      </Col>

</Row>
 {/*page*/}
              <div className="cntrl">
                <Pagination className="kol">
                 <Pagination.First />
                 <Pagination.Prev />
                  <Pagination.Item active>{1}</Pagination.Item>
                  <Pagination.Item>{2}</Pagination.Item>
                 <Pagination.Item>{3}</Pagination.Item>

                 <Pagination.Ellipsis />
                 <Pagination.Item>{69}</Pagination.Item>
                 <Pagination.Next />
                 <Pagination.Last />
                </Pagination>
              </div>
    
    </Container>
    );
}
function Login(){
  return(
     
    <div className="lgn">
      <Container className="lgn">
      <Row className="kol">
      <Col>
              <h5> 📫 Newsletter.. </h5>
              <p> Dapatkan informasi terbaru dari petanikode dengan berlangganan Newsletter. </p>
                   <Form>
                 <Form.Group controlId="formBasicEmail">
                   <Form.Label>Nama</Form.Label>
                   <Form.Control type="text" placeholder="Nama Panggilan" />
                 </Form.Group>
                
                 <Form.Group controlId="formBasicPassword">
                   <Form.Label>Email</Form.Label>
                    <Form.Control type="Email" placeholder="Alamat Email" />
                 </Form.Group>
              
                 <Button type="submit" className="tombol">
                   Daftar
                  </Button>
                  <p>Atau ikuti kami melalui:</p>
                  <img src={facebook} width={30} height={30} />
                  <img src={ig} width={30} height={30} style={{paddingLeft: '5px'}}/>
                  <img src={tweeter} width={55} height={30} />
                </Form>               

      </Col>

      <Col>
      <img className="gmbr" src="https://www.petanikode.com/img/newsletter.svg" alt=""/>
      </Col>



      </Row>
      </Container>
    </div>
    );
}
function Review(){
  return(
    <div>
    <Container>
    <div className="head1">
    <h3> 
      Artikel Berdasarkan Kategori
    </h3>
    <h5>
      Baca artikel yang sesuai dengan minatmu
    </h5>
    </div>

    <Row>
     {/*baris kesatu*/}
        <Col>
          <Card border="light">
          <h5>Database</h5>
          <br/>
          <Card.Body>
    <Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Tutorial Codeigniter #5: Cara Membuat Fitur CRUD yang Benar!</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://pbs.twimg.com/media/ED0p9L8WkAYZPcX.png"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Belajar Nodejs #12: Menggunakan Database SQLite pada Nodejs</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://lh3.googleusercontent.com/nin_vXeS6JlsCU-vH1zIUw0ozAuJJIKE6LgkC8znKzOcJ6VmDR5MW1Kx5XZyq2j2fzCk4g=s85"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Belajar Nodejs #9: Cara Menggunakan Database MySQL pada Nodejs</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://lh3.googleusercontent.com/nin_vXeS6JlsCU-vH1zIUw0ozAuJJIKE6LgkC8znKzOcJ6VmDR5MW1Kx5XZyq2j2fzCk4g=s85"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Tutorial PHP & MySQL: Membuat Login dan Register (dengan Bootstrap 4)</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://lh3.googleusercontent.com/uaKfooakTB85l5IPu0f2ssKEMg5VlXIVAlk7fxrYE-YgV5zSDe5OjEbqAmd_vb9JgP5bpEc=s85"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Tutorial Java dan MySQL: Membuat Program CRUD Berbasis Teks</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://lh3.googleusercontent.com/fKuMrK2fU4KdVFVarjR8muKBUt493ajzE4li9q96kY9D_H2W3vBTtt-AMdh4HXtQclkrHg=s85"
    alt="" rounded
  />
  </a>
</Media>
<small>
<a href="#home">Lihat Semua ➜</a>
</small>
  </Card.Body>
          </Card>
        </Col>
 {/*baris ke2*/}

        <Col>
        <Card border="light">
          <h5>Dekstop</h5>
          <br/>
          <Card.Body>
    <Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Belajar Membuat Aplikasi GUI Berbasis Python dan GTK dengan PyGObject</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://lh3.googleusercontent.com/OkTG0rDZKgkV4q54TAr2VbrRYXdnQfxwe0SSuj2BKlYxERiW6FoNdbE56jDTmlt9bsW-=s85"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Membuat Aplikasi GUI berbasis Python dengan PyGTK dan Glade</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://www.petanikode.com/img/cover/pygtk-sqr.png"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Tutorial Membuat Aplikasi Desktop dengan Flutter</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://injouri.ir/wp-content/uploads/2019/01/Flatter-v.2-600x600.jpg"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>9 Framework Javascript untuk Membuat Aplikasi GUI/Desktop</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX33x4AAAD/5h/85B9qYA2rmxX/6R//6h/64R6/rBfaxRu6qBeOgBFFPghLRAn13R7hyxuzohYZFgOmlhTOuhlTSwro0Rzt1h1eVQuYiRJmXAwrJwU2MQbFshjmzxyfjxODdhB5bQ9SSgqAdBByZw7TvxokIQQ/OQgRDwI7NgcbGAMfHAQqJgUzLgaKfBELCgE3LjMCAAAG1ElEQVR4nO2c21qjPBSGIdGQ7qE7itpq1doZN/X+7+6HWh1aViCh0MTn/96DmQMRecl+sRLPAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABqJOeMMZGR/s+5tP1ABNkTFtB6UJn+athL1ovt+/jP28dgsU56XS5cs+TJzVWBm071U3Ixnyz8IsOkK/gFHlwbdkM8pX9d9YyMRwPqF/eMk1ivFlwEdlXDkHuJUu+L5zm7kEAlNQylmFT4ZeykI3XV3JCFHxqCKVNxOY0SjA2Daz2/lL4TiqaGYq0t6PsLFxqjoaEgu14lAwfaopmhoWCqaL8UjQzFg6FgWlGtt0UTQ94zFkxHDduKBoYyriHo+x3LbdHAMJjVMvRjuzM4fUO+rCfoPwaX18qhbyhuaxr6Pav1VNtQ1i3ClJEFsR+0DUXNVpiSWPD6h66hnKsNhk9J0l+r1ouDld1RX9eQq1ZMr5EnGOecCdmjJK+F5cWwriFTVNId+3cpDwptdRFbn7ZpG9KCk+ORgI+Oi7Fjd6DYo2kou6Tg86mBZMPcTz0Hlha6hoopKTEM8PfDz8a/a43PydDTE9HIvjvdHXehAD1tQ/ZEXUZOqlkW5vjoWu9hvtE1JC8LyYEgGPj3toeIHLqG5OI+Jm8pu6EzBeidaTinS0q6U4DembV045SKAl3DNXXZxJHushRdwx112dCBKUsluuMhPfH+DdVU17BDGr78gmqqOy8NSUP/0aEPhQrOXFv425HrxXju+tD3e4Hbxai9xr9XGfrDrkNztCLacRpFQ9xz47KjfqytNFw6mwautkdtQ2Uo6sBHxN3sVw2+W5QbpvRjFyurviGryjBJudm4V1lNvq5VG/r+XcQcczQw1P1Aei9dWgAbfQMOqDw2isRzyNEsU0HTMHV0JSPK0LB02D+9hSv9qlkZ8qm+4u3mV0WEfy43+U76q6L6/643KEXfXzpQjMaZezz8Y6D4YH8mZ559KT3dQSNjMLKtWCdH2CAB0/c/578nYyj3S/GjgWNoV7GWoSeDzqe+4spqRa1nmHY4JWGNAlbbYl3D9DcrE/Z/2NocNOobZo665di3OBM/xzDbUhTdaSl27dXT8wyzHJoNeYcT7uzV03MNsw0mcb9a0V5+4vmG3n4XVNUuk1trhdiIYbaTbVqRuzi11RIbMswqa/hcZvhsqzttzDBzXJV1OrYaYoOGmWP3r9JwaamaNmqYOSonAX1LhdiwYXrDjcLw0VJDbNzQ46qAnKXxonlDj9FZDYocsdZpwVC1ucZSakobhpKOx1naAGVsqPPZhU7ciNo1VNUQ0/34YnP7UNllMDKM02oWnOR0sqsqi0T1ulmcFXnlYo+R87c2DdMXP1ZULjoBgW4y/JDtPa48ceHChnyU/cEJrUg3GWohIIPOd7T7qiInkZEx4/O6LzVSHOK35AYyxfcyoh6KMLeT4r68KQrypi0tgkX3O5ByQ714ToevC2+Dj9bHT1vWoypGi1ZWiNzLPRj1DhV77k6KSLJCMk2nRFHxNVzV2Z2BFNHRnygmnssV+SzDo8eXwfSleM21si0yRUZD85WUhdvjP/FaiD0r3vZD3pDN6YvWirQSRm+Raj4szGXxeID3+PihAsViLj8ccjLDO+NtSuRdpj2u4vKHZldPMqCryjL3UFyoooD56kz3/F8sNsHxqVBSxMpAhsbJTCaC+a79iNl0fyYXY8KL3hXXvOTrU9n2WN+/m4Qiux//2kW6LAnUNNvP0JOKA4tdtm13q77gON7AquK9i90k6vWi+yfVW91z1fASv94BDwdOxnv2ds7Nvml6NKx88SWcfmNQ7B8146PxGIZWIiFNYWGhk3dZRfPLX8UWEA0+iw1GmGRfkAxaCEMJnY9eFNTakNc+E+NACzO2tJ6Oaz0L+aVPjgwSEwiSVmKlJomEOei3XfP0nQOzlkKltQ6UUSyUU8V6VSLjpbUwomqSX4J69S5HJXOEUsYtJpsYKw5L2ouk95FW8t5qNo1ZrqQ/LB+0FLP5cmYt73vmK2LxqqJy6shj42N4kta/x0i+1n2YSfWu3ly8TYvLHK0gNqpV0hFbvaOAeDFko+ZS2exSRJUv/rWn/TC8OrVkz+fkgmd/cEYecPTDoGe0lYeLDXkQSJ5F58K7g7gIE0USwd8kND68WTI27auTEq6ikYXzoCUXcae/OKqv40W/E9c8gDu932h6f3rC13i2i0Jhb3PXPpay2kyXnc5y2p3z7JCuc+4n9/GeVTe74fcdmf0Ty2UK59m/jd6QN3dDAAAAAAAAAAD/M/4DiYFShyvaDPIAAAAASUVORK5CYII="
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>12 Modul Python untuk Membuat Aplikasi GUI (Desktop)</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://turinghack.com/wp-content/uploads/2020/10/check-python-version-in-windows-macos-linux.png"
    alt="" rounded
  />
  </a>
</Media>
<small>
<a href="#home">Lihat Semua ➜</a>
</small>
  </Card.Body>
          </Card>

        </Col>
 {/*baris ke3*/}
        <Col>
        <Card border="light">
          <h5>Game</h5>
          <br/>
          <Card.Body>
    <Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Belajar Pemrograman Game dengan PyGame (Tutorial Step-by-step untuk Pemula)</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://lh3.googleusercontent.com/zGhkbs5KQnN5x2BTyxNud2t-578iQEKOrFI25SOWHdEyx9ybg48JNDIwAJEyRzEb9-Eq_EQ=s85"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Pemrograman Game di Linux Menggunakan Unity3D, Apakah Bisa?</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://lh3.googleusercontent.com/HZ2cGsqCXTF5F_r0Cp5wmIj77H9AVVgmqoJtP76ioPMNn9xd9ucd0JYLS8tu0Pn7ib3cSYI=s85"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Menggambar Objek dengan Perulangan dan Fungsi Random di HTML5 Canvas</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://www.petanikode.com/img/html-canvas/html5-canvas-square.png"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Mengenal HTML5 Canvas untuk Pemrograman Grafis dan Game</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://www.petanikode.com/img/html-canvas/html5-canvas-square.png"
    alt="" rounded
  />
  </a>
</Media>

<hr/>

<Media>
  <Media.Body> 
  <a href="#home">
    <h5 style={{color:'black'}}>Menulis Kode Serasa Main Game dengan Modul Activate Power Mode di Atom</h5>
    </a>
  </Media.Body>
  <a href="#home">
  <img
    width={64}
    height={64}
    className="ml-3"
    src="https://previews.123rf.com/images/fokaspokas/fokaspokas1805/fokaspokas180500424/102128423-scientific-atom-symbol-logo-simple-icon.jpg"
    alt="" rounded
  />
  </a>
</Media>
<small>
<a href="#home">Lihat Semua ➜</a>
</small>
  </Card.Body>
          </Card>

        </Col>
    </Row>
    </Container>
    </div>
    );
}
function Footer(){
  return(
    <div className="kol1">
  {/* tentang petani kode */}
    <Container>
        <Row>
          
          <Col xs={6} md={4}> 
          <img 
          src="https://www.petanikode.com/img/about-section.svg" 
          alt="" 
          width="100%" 
          />

          </Col>
            
          <Col xs={12} md={8}> 
          <h2>Tentang Petani Kode </h2>
            <p>Petani Kode adalah blog yang membahas seputar dunia pemrograman dan Linux.
               Tersedia tips dan panduan pemrograman menggunakan Linux yang bisa diikuti
               oleh siapa saja yang tertarik melakukan pemrograman menggunakan Linux.
                Tidak menutup kemungkinan juga, ada beberapa panduan yang menggunakan Windows.</p><a href="#home">Baca Selengkapnya...</a>
         <hr/>
          <Button href="#home" className="jarak">Join Group!</Button>
          <Button variant="outline-info" href="#home">Join Group!</Button>
          </Col>

        </Row>
    </Container>
{/* footer */}
  <div className="lgn1">
    <Container className="lgn">
          <div className="icon" col-12 text-center my3>
          © 2021<a href="#home"><strong>Petani Kode</strong></a>
          <span>| Made with ❤️ using </span>
          <a href="#home">Hugo 0.78.2</a>
        </div>
      </Container>
  </div>
    </div>
    );
}

function App() {
  return (
    <div>
    <Header/>
    <Welcome/>
    <Login/>
    <Review/>
    <Footer/>
    </div>
  );
}

export default App;
